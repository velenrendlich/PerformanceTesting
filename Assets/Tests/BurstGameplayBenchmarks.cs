using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using static Unity.Burst.CompilerServices.Hint;

namespace PerformanceTests
{
	[BurstCompile]
	public static unsafe class BurstGameplayBenchmarks
	{
		const int UnitCount = 5000;
		const int IterationCount = 20;

		public struct Unit
		{
			public int Id;

			[MarshalAs(UnmanagedType.U1)]
			public bool IsDead;

			public int Faction;

			public float2 Position;

			public int EnemyUnitId;
		}

		[BurstCompile]
		public static void CreateRandomUnit(int id, ref Random random, out Unit unit)
		{
			unit = new Unit
			{
				Id = id,
				IsDead = random.NextFloat() > 0.1f, // 10 percent of units are dead
				Faction = random.NextInt(1, 10), // 10 Different factions 
				Position = new float2(random.NextFloat(10f), random.NextFloat(10f)), // Random position on the map
			};
		}

		[BurstCompile]
		static void FindClosestUnit(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				if(otherUnit.Id == unit.Id)
					continue;
				if(otherUnit.IsDead)
					continue;
				if(otherUnit.Faction == unit.Faction)
					continue;

				var distance = math.distance(unit.Position, otherUnit.Position);
				if(distance <= radius)
				{
					if(distance < closestUnitDist)
					{
						closestUnitDist = distance;
						closestUnitId = otherUnit.Id;
					}
				}
			}

			unit.EnemyUnitId = closestUnitId;
		}			
		
		[BurstCompile]
		static void FindClosestUnit_DistSq_Intrinsics(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				if(Unlikely(otherUnit.Id == unit.Id))
					continue;
				if(Unlikely(otherUnit.IsDead))
					continue;
				if(Unlikely(otherUnit.Faction == unit.Faction))
					continue;

				var distance = math.distancesq(unit.Position, otherUnit.Position);
				if(distance <= radius)
				{
					if(distance < closestUnitDist)
					{
						closestUnitDist = distance;
						closestUnitId = otherUnit.Id;
					}
				}
			}

			unit.EnemyUnitId = closestUnitId;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[BurstCompile]
		static void FindClosestUnit_DistSq_Intrinsics_Inlined(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				if(Unlikely(otherUnit.Id == unit.Id))
					continue;
				if(Unlikely(otherUnit.IsDead))
					continue;
				if(Unlikely(otherUnit.Faction == unit.Faction))
					continue;

				var distance = math.distancesq(unit.Position, otherUnit.Position);
				if(distance <= radius)
				{
					if(distance < closestUnitDist)
					{
						closestUnitDist = distance;
						closestUnitId = otherUnit.Id;
					}
				}
			}

			unit.EnemyUnitId = closestUnitId;
		}
		
		[BurstCompile(FloatPrecision.Low, FloatMode.Fast)]
		static void FindClosestUnit_DistSq_Intrinsics_FastFloat(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				if(Unlikely(otherUnit.Id == unit.Id))
					continue;
				if(Unlikely(otherUnit.IsDead))
					continue;
				if(Unlikely(otherUnit.Faction == unit.Faction))
					continue;

				var distance = math.distancesq(unit.Position, otherUnit.Position);
				if(distance <= radius)
				{
					if(distance < closestUnitDist)
					{
						closestUnitDist = distance;
						closestUnitId = otherUnit.Id;
					}
				}
			}

			unit.EnemyUnitId = closestUnitId;
		}
		
		[BurstCompile]
		static void FindClosestUnit_DistSq_FalseIntrinsics(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				if(Unlikely(otherUnit.Id == unit.Id))
					continue;
				if(Unlikely(otherUnit.IsDead))
					continue;
				if(Unlikely(otherUnit.Faction == unit.Faction))
					continue;

				var distance = math.distancesq(unit.Position, otherUnit.Position);
				if(Unlikely(distance <= radius))
				{
					if(Unlikely(distance < closestUnitDist))
					{
						closestUnitDist = distance;
						closestUnitId = otherUnit.Id;
					}
				}
			}

			unit.EnemyUnitId = closestUnitId;
		}
		
		[BurstCompile]
		static void FindClosestUnit_DistSq(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				if(otherUnit.Id == unit.Id)
					continue;
				if(otherUnit.IsDead)
					continue;
				if(otherUnit.Faction == unit.Faction)
					continue;

				var distance = math.distancesq(unit.Position, otherUnit.Position);
				if(distance <= radius)
				{
					if(distance < closestUnitDist)
					{
						closestUnitDist = distance;
						closestUnitId = otherUnit.Id;
					}
				}
			}

			unit.EnemyUnitId = closestUnitId;
		}	
		
		[BurstCompile]
		static void FindClosestUnit_DistSq_NoBranch(ref Unit unit, Unit* units, int unitCount, float radius)
		{
			var closestUnitId = -1;
			var closestUnitDist = float.PositiveInfinity;
			
			for(int i = 0; i < unitCount; i++)
			{
				var otherUnit = units[i];

				var hasSameId = otherUnit.Id == unit.Id;
				var isDead = otherUnit.IsDead;
				var hasSameFaction = otherUnit.Faction == unit.Faction;
				var distance = math.distancesq(unit.Position, otherUnit.Position);
				var isOutOfRadius = distance >= radius;
				var distanceIsGreater = distance > closestUnitDist;
				var shouldSkip = hasSameId || isDead || hasSameFaction || isOutOfRadius || distanceIsGreater;
				closestUnitDist = shouldSkip ? closestUnitDist : distance;
				closestUnitId = shouldSkip ? closestUnitId : otherUnit.Id;
			}

			unit.EnemyUnitId = closestUnitId;
		}	

		[BurstCompile]
		static void FindEnemyUnits(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				if(units[i].IsDead)
					continue;
				
				FindClosestUnit(ref units[i], units, unitCount, radius);
			}
		}
		
		[BurstCompile]
		static void FindEnemyUnits_DistSq_Intrinsics(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				if(Unlikely(units[i].IsDead))
					continue;
				
				FindClosestUnit_DistSq_Intrinsics(ref units[i], units, unitCount, radius);
			}
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[BurstCompile]
		static void FindEnemyUnits_DistSq_Intrinsics_Inlined(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				if(Unlikely(units[i].IsDead))
					continue;
				
				FindClosestUnit_DistSq_Intrinsics_Inlined(ref units[i], units, unitCount, radius);
			}
		}
		
		[BurstCompile(FloatPrecision.Low, FloatMode.Fast)]
		static void FindEnemyUnits_DistSq_Intrinsics_FastFloat(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				if(Unlikely(units[i].IsDead))
					continue;
				
				FindClosestUnit_DistSq_Intrinsics_FastFloat(ref units[i], units, unitCount, radius);
			}
		}
		
		[BurstCompile]
		static void FindEnemyUnits_DistSq_Intrinsics_Extra(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				if(Unlikely(units[i].IsDead))
					continue;
				
				FindClosestUnit_DistSq_FalseIntrinsics(ref units[i], units, unitCount, radius);
			}
		}
		
		[BurstCompile]
		static void FindEnemyUnits_DistSq(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				if(units[i].IsDead)
					continue;
				
				FindClosestUnit_DistSq(ref units[i], units, unitCount, radius);
			}
		}
		
		[BurstCompile]
		static void FindEnemyUnits_DistSq_NoBranch(Unit* units, int unitCount, float radius)
		{
			for(int i = 0; i < unitCount; i++)
			{
				FindClosestUnit_DistSq_NoBranch(ref units[i], units, unitCount, radius);
			}
		}
		
		[Test, Performance]
		public static void ClosestUnit_DistSq_Intrinsics_Inlined()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits_DistSq_Intrinsics_Inlined(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void ClosestUnit_DistSq_Intrinsics_FastFloat()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits_DistSq_Intrinsics_FastFloat(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void ClosestUnit_DistSq_Intrinsics()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits_DistSq_Intrinsics(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void ClosestUnit_DistSq_Intrinsics_Extra()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits_DistSq_Intrinsics_Extra(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		// [Test, Performance]
		public static void ClosestUnit_DistSq_NoBranch()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits_DistSq_NoBranch(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
		
		[Test, Performance]
		public static void ClosestUnit_DistSq()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits_DistSq(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}

		[Test, Performance]
		public static void ClosestUnit_Distance()
		{
			NativeArray<Unit> units = default;

			Measure.Method(() =>
			       {
				       Unit* unitPtr = (Unit*)units.GetUnsafePtr();
				       FindEnemyUnits(unitPtr, UnitCount, 3f);
			       })
			       .SetUp(() =>
			       {
				       var seed = (uint)Stopwatch.GetTimestamp();
				       var random = new Random(seed);

				       units = new NativeArray<Unit>(UnitCount, Allocator.Temp);
				       for(int i = 0; i < units.Length; i++)
				       {
					       CreateRandomUnit(i, ref random, out var unit);
					       units[i] = unit;
				       }
			       })
			       .CleanUp(() => { units.Dispose(); })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .Run();
		}
	}
}