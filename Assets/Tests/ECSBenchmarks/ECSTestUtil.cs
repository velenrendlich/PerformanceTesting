using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Core;
using Unity.Entities;

namespace PerformanceTests
{
	public static class ECSTestUtil
	{
		public const float SimulationDeltaTime = 0.05f;

		public static World CreateEditorWorld()
		{
			return new World("TestWorld", WorldFlags.Editor);
		}

		public static void AddSystemsToWorld(World world, List<Type> systemTypes)
		{
			DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(world, systemTypes);
		}

		public static void TickWorld(World world, float deltaTime = SimulationDeltaTime)
		{
			FastForwardTime(world, deltaTime);
			world.Update();
			world.EntityManager.CompleteAllJobs();
		}

		static void FastForwardTime(World world, float duration)
		{
			var currentTime = world.Time;
			world.Time = new TimeData(currentTime.ElapsedTime + duration, duration);
		}

		public static T AddSystemToWorld<T>(World world) where T : ComponentSystemBase
		{
			AddSystemsToWorld(world, new List<Type> { typeof(T) });
			return world.GetExistingSystem<T>();
		}

		public static NativeArray<Entity> CreateEntities(World world, int entityCount, Allocator allocator,
		                                                 params ComponentType[] types)
		{
			var entityManager = world.EntityManager;
			var archetype = entityManager.CreateArchetype(types);
			return entityManager.CreateEntity(archetype, entityCount, allocator);
		}

		public static void SetComponent<T>(World world, Entity entity, T component) where T : struct, IComponentData
		{
			world.EntityManager.SetComponentData(entity, component);
		}
	}
}