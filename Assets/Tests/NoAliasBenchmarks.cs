using NUnit.Framework;
using Unity.Burst;
using Unity.PerformanceTesting;
using UnityEngine;

namespace PerformanceTests
{
	[BurstCompile]
	public static unsafe class NoAliasBenchmarks
	{
		[BurstCompile]
		public static void IncrementBurstNoAlias([NoAlias]
		                                         int* source, [NoAlias]
		                                         int* destination, int count)
		{
			for(int i = 0; i < count; i++)
			{
				source[i] += destination[i];
			}
		}

		[BurstCompile]
		public static void IncrementBurst(int* source, int* destination, int count)
		{
			for(int i = 0; i < count; i++)
			{
				source[i] += destination[i];
			}
		}

		public static void Increment(int* source, int* destination, int count)
		{
			for(int i = 0; i < count; i++)
			{
				source[i] += destination[i];
			}
		}

		public static void IncrementNoPointer(int[] source, int[] destination, int count)
		{
			for(int i = 0; i < count; i++)
			{
				source[i] += destination[i];
			}
		}

		const int Count = 100000;

		static void InitRandomValues(int[] array)
		{
			for(int i = 0; i < array.Length; i++)
			{
				array[i] = Random.Range(0, 10000);
			}
		}

		[Test, Performance]
		public static void MeasureIncrementBurstNoAlias()
		{
			var source = new int[Count];
			var destination = new int[Count];
			InitRandomValues(source);
			InitRandomValues(destination);

			Measure.Method(() =>
			       {
				       fixed(int* sourcePtr = source)
				       fixed(int* destinationPtr = destination)
				       {
					       IncrementBurstNoAlias(sourcePtr, destinationPtr, Count);
				       }
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void MeasureIncrementBurst()
		{
			var source = new int[Count];
			var destination = new int[Count];
			InitRandomValues(source);
			InitRandomValues(destination);

			Measure.Method(() =>
			       {
				       fixed(int* sourcePtr = source)
				       fixed(int* destinationPtr = destination)
				       {
					       IncrementBurst(sourcePtr, destinationPtr, Count);
				       }
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void MeasureIncrement()
		{
			var source = new int[Count];
			var destination = new int[Count];
			InitRandomValues(source);
			InitRandomValues(destination);

			Measure.Method(() =>
			       {
				       fixed(int* sourcePtr = source)
				       fixed(int* destinationPtr = destination)
				       {
					       Increment(sourcePtr, destinationPtr, Count);
				       }
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void MeasureIncrementNoPointer()
		{
			var source = new int[Count];
			var destination = new int[Count];
			InitRandomValues(source);
			InitRandomValues(destination);

			Measure.Method(() => { IncrementNoPointer(source, destination, Count); })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
	}
}