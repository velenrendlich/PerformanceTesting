using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using Unity.Burst;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace PerformanceTests
{
	[BurstCompile]
	public static unsafe class CacheHitBenchmarks
	{
		struct SomeStructData
		{
			public float4 Value1;
			public float4 Value2;
		}

		class SomeClassData
		{
			public float4 Value1;
			public float4 Value2;
		}

		const int Count = 10000;

		[Test, Performance]
		public static void MeasureStructList()
		{
			var structList = new List<SomeStructData>(Count);
			var random = new Random((uint)Stopwatch.GetTimestamp());
			
			for(int i = 0; i < Count; i++)
			{
				var randomStruct = new SomeStructData
				{
					Value1 = random.NextFloat4(),
					Value2 = random.NextFloat4()
				};
				
				structList.Add(randomStruct);
			}
			
			Measure.Method(() =>
			       {
				       for(int i = 0; i < structList.Count; i++)
				       {
					       var str = structList[i];
					       str.Value1 += str.Value2;
					       structList[i] = str;
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
		
		// Well I'll be damned, TIL we can't use list of pointers in C#, so this performance test is useless.
		// [Test, Performance]
		// public static void MeasureStructPtrList()
		// {
		// 	var structList = new List<SomeStructData*>(Count);
		// 	var random = new Random((uint)Stopwatch.GetTimestamp());
		// 	
		// 	for(int i = 0; i < Count; i++)
		// 	{
		// 		var randomStruct = new SomeStructData
		// 		{
		// 			Value1 = random.NextFloat4(),
		// 			Value2 = random.NextFloat4()
		// 		};
		// 		
		// 		structList.Add(randomStruct);
		// 	}
		// 	
		// 	Measure.Method(() =>
		// 	       {
		// 		       for(int i = 0; i < structList.Count; i++)
		// 		       {
		// 			       var str = structList[i];
		// 			       str.Value1 += str.Value2;
		// 			       structList[i] = str;
		// 		       }
		// 	       })
		// 	       .WarmupCount(10)
		// 	       .MeasurementCount(10)
		// 	       .IterationsPerMeasurement(10)
		// 	       .Run();
		// }
		
		[Test, Performance]
		public static void MeasureClassList()
		{
			var classList = new List<SomeClassData>(Count);
			var random = new Random((uint)Stopwatch.GetTimestamp());
			
			for(int i = 0; i < Count; i++)
			{
				var randomStruct = new SomeClassData
				{
					Value1 = random.NextFloat4(),
					Value2 = random.NextFloat4()
				};
				
				classList.Add(randomStruct);
			}
			
			Measure.Method(() =>
			       {
				       for(int i = 0; i < classList.Count; i++)
				       {
					       var cls = classList[i];
					       cls.Value1 += cls.Value2;
					       classList[i] = cls;
				       }
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
	}
}